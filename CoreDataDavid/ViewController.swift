//
//  ViewController.swift
//  CoreDataDavid
//
//  Created by david polania on 1/13/20.
//  Copyright © 2020 polania. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {

    //MARK:constant-controls
    @IBOutlet weak var tableData: UITableView!
    var personas : [Persona]?
    
    //MARK:variables
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK:Ciclo de vida
    override func viewDidLoad() {
        super.viewDidLoad()
        tableData.dataSource = self
        tableData.delegate   = self
        // Do any additional setup after loading the view.
    }
    
    //MARK:Metodos y funciones
    /**
     Funcion encargada de la creacion de los primeros datos de coredata
     */
    fileprivate func createData() {
        
        //Primera forma de guardar datos con set value
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Persona", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        newUser.setValue(26, forKey: "edad")
        newUser.setValue("Juan", forKey: "nombre")
        newUser.setValue("Rodriguez", forKey: "apellido")
        newUser.setValue("Contador antiguo", forKey: "descripcion")
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
        
        //Segunda forma de guardar datos con set value
        let model : Persona = Persona(context: context)
        model.edad          = 28
        model.nombre        = "David"
        model.apellido      = "Polania"
        model.descripcion   = """
        Ingeniero de sistemas
        Actualmete trabjando en valid
        """
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    func retrieveData( persona: inout [Persona]?) throws{
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Persona")
        do{
            let result = try context.fetch(fetchRequest) as? [Persona]
            persona = result ?? [Persona(context: context)]
        }
        catch{
            print("error")
        }
    }
    
    func finData() -> [Persona]{
        var persona : [Persona]?
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Persona")
        fetchRequest.predicate = NSPredicate(format: "nombre = %@", "David")
        do{
            let result = try context.fetch(fetchRequest) as? [Persona]
            persona = result ?? [Persona(context: context)]
            return persona ?? [Persona(context: context)]
        }
        catch{
            print("error")
            return persona ?? [Persona(context: context)]
        }
    }
    
    func deleteDataTable(){
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Persona")
        fetchRequest.predicate = NSPredicate(format: "nombre = %@", "David")
        do{
            let result = try context.fetch(fetchRequest) as? [Persona]
            context.delete(result?.first ?? Persona(context: context))
            try context.save()
        }
        catch{
            print("error")
        }
    }

    fileprivate func countPerson() -> Int{
        do{
            var model : [Persona]?
            try retrieveData(persona: &model)
            personas = model
            return model?.count ?? 0
        }catch{
            return 0
        }
    }
    @IBAction func actionFind(_ sender: Any) {
        let resultado = finData()
        print(resultado)
        tableData.reloadData()
    }
    
    @IBAction func deleteData(_ sender: Any) {
        deleteDataTable()
        tableData.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countPerson()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellData") as! DataViewCell
        cell.labelName.text         = "Nombre: \(self.personas?[indexPath.row].nombre ?? "")"
        cell.labelLastName.text     = "Apellido: \(self.personas?[indexPath.row].apellido ?? "")"
        cell.labelYearOld.text      = "Años: \(self.personas?[indexPath.row].edad ?? 0)"
        cell.labelDescription.text  = "Descripcion: \(self.personas?[indexPath.row].descripcion ?? "")"
        return cell
    }
   
}

